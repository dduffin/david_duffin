﻿using PostCodeAPI.Annotations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PostCodeAPI.Models
{
    public class PostCodeModel
    {
        [Required]
        public int id { get; set; }
        [Required]
        [RequiredLength(4)]
        [IntegerOnly]
        public string PostCode { get; set; }
        [Required]
        [StringLength(100)]       
        public string Suburb { get; set; }

    }
}