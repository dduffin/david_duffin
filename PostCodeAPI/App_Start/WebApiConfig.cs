﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
 
namespace PostCodeAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

          
            config.Routes.MapHttpRoute(
               name: "DefaultApi",
               routeTemplate: "api/{controller}/{action}"
          
              
           );

            config.Routes.MapHttpRoute(
               name: "SearchID",
               routeTemplate: "api/{controller}/{action}/{id}"              

           );

            config.Routes.MapHttpRoute(
               name: "SearchSuburb",
               routeTemplate: "api/{controller}/{action}/{suburb}" 
               );

            config.Routes.MapHttpRoute(
               name: "SearchSuburbPostCode",
               routeTemplate: "api/{controller}/{action}/{suburb}/{postcode}"
             

               );

        }
    }
}
