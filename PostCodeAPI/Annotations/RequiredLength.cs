﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PostCodeAPI.Annotations
{
    public class RequiredLength: ValidationAttribute
    {
       private  int ReqLength = 0;
        public RequiredLength(int length)
        {
            ReqLength = length;
        }

        public override bool IsValid(object value)
        {
            string strValue = value as string;
            if (!string.IsNullOrEmpty(strValue))
            {
                return strValue.Length == ReqLength;
            }
            else
            {
                return false;
            }


        }
    }
}