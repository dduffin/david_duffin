﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PostCodeAPI.Annotations
{
    public class IntegerOnly : ValidationAttribute
    {

        public IntegerOnly()
        {

        }

        public override bool IsValid(object value)
        {
            string strValue = value as string;
            if (!string.IsNullOrEmpty(strValue))
            {
                int tmp;
                return int.TryParse(strValue, out tmp);


            }
            else
            {
                return false;
            }


        }
    }
}