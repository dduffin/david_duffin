﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PostCodeAPI.Models
{
    public class PostCodeController : System.Web.Http.ApiController
    {
        List<PostCodeModel> postcodes;

        private List<PostCodeModel> PopulateAllPostCodes()
        {
            if (postcodes == null)
            {
                postcodes = new List<PostCodeModel>();
            }

            using (System.IO.StreamReader sr = new System.IO.StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/postcodes.csv")))
            {

                int lnNo = 0;
                while (sr.Peek() != -1)
                {
                    var ln = sr.ReadLine().Split(',');
                    if (lnNo != 0)
                    {

                        if (ln.Length == 3)
                        {
                            postcodes.Add(new PostCodeModel() { id = int.Parse(ln[0]), Suburb = ln[1], PostCode = ln[2] });
                        }
                    }
                    lnNo += 1;

                }
            }

            return postcodes;

        }

        [Route("api/PostCode/GetPostCodes")]
        [System.Web.Http.HttpGet]
        public IEnumerable<PostCodeModel> GetPostCodes()
        {
            try
            {
                return PopulateAllPostCodes();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.Write(ex.ToString());
                throw new System.Web.Http.HttpResponseException(new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.InternalServerError));
            }
        }

        [Route("api/PostCode/GetPostCodes/{id}")]
        [System.Web.Http.HttpGet]
        public IEnumerable<PostCodeModel> GetPostCodes(int id)
        {
            try
            {
                return PopulateAllPostCodes().Where(x => x.id == id);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.Write(ex.ToString());
                throw new System.Web.Http.HttpResponseException(new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.InternalServerError));
            }
        }

        [Route("api/PostCode/SearchPostCodes/{suburb}")]
        [System.Web.Http.HttpGet]
        public IEnumerable<PostCodeModel> SearchPostCodes(String Suburb)
        {
            try
            {
                var lstPC = PopulateAllPostCodes().Where(x => (!string.IsNullOrWhiteSpace(Suburb) && (x.Suburb.ToLower().Contains(Suburb.ToLower()))));

                return lstPC;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.Write(ex.ToString());
                throw new System.Web.Http.HttpResponseException(new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.InternalServerError));

            }
        }

        [Route("api/PostCode/SearchPostCodes/{suburb}/{PostCode}")]
        [System.Web.Http.HttpGet]
        public IEnumerable<PostCodeModel> SearchPostCodes(String Suburb, string PostCode)
        {
            try
            {
                var lstPC = PopulateAllPostCodes().Where(x => (!string.IsNullOrWhiteSpace(PostCode) && (x.PostCode.Contains(PostCode)) || (string.IsNullOrWhiteSpace(PostCode))));
                if (!string.IsNullOrWhiteSpace(Suburb))
                {
                    lstPC = lstPC.Where(x => (!string.IsNullOrWhiteSpace(Suburb) && (x.Suburb.ToLower().Contains(Suburb.ToLower()))));
                }
                return lstPC;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.Write(ex.ToString());
                throw new System.Web.Http.HttpResponseException(new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.InternalServerError));

            }
        }

        [System.Web.Http.HttpPut]
        public void UpdatePostCode(PostCodeModel PC)
        {
            if (ModelState.IsValid)
            {
                bool updated = false;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                using (System.IO.StreamReader sr = new System.IO.StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/postcodes.csv")))
                {
                    int lnNo = 0;
                    while (sr.Peek() != -1)
                    {
                        var ln = sr.ReadLine().Split(',');
                        if (lnNo != 0)
                        {
                            if (ln[0] == PC.id.ToString())
                            {
                                ln[1] = PC.Suburb;
                                ln[2] = PC.PostCode;
                                updated = true;
                            }
                            sb.AppendLine(String.Join(",", ln));

                        }
                        else
                        {
                            sb.AppendLine(String.Join(",", ln));
                        }
                        lnNo += 1;

                    }
                }

                if (updated == true)
                {
                    using (System.IO.StreamWriter sw = new System.IO.StreamWriter(System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/postcodes.csv")))
                    {
                        sw.Write(sb.ToString());
                    }
                }
            }
            else
            {
                throw new System.Web.Http.HttpResponseException(new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.InternalServerError));
            }
        }

        [System.Web.Http.HttpDelete]
        public void DeletePostCode(PostCodeModel PC)
        {
            if (ModelState.IsValid)
            {
                bool updated = false;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                using (System.IO.StreamReader sr = new System.IO.StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/postcodes.csv")))
                {
                    int lnNo = 0;
                    while (sr.Peek() != -1)
                    {
                        var ln = sr.ReadLine().Split(',');
                        if (lnNo != 0)
                        {
                            if (ln[0] == PC.id.ToString())
                            {
                                updated = true;
                            }
                            else
                            {
                                sb.AppendLine(String.Join(",", ln));
                            }
                        }
                        else
                        {
                            sb.AppendLine(String.Join(",", ln));
                        }
                        lnNo += 1;

                    }
                }

                if (updated == true)
                {
                    using (System.IO.StreamWriter sw = new System.IO.StreamWriter(System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/postcodes.csv")))
                    {
                        sw.Write(sb.ToString());
                    }
                }


            }
            else
            {
                throw new System.Web.Http.HttpResponseException(new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.InternalServerError));
            }
        }

        [System.Web.Http.HttpPost]
        public void CreatePostCode(PostCodeModel PC)
        {
            if (ModelState.IsValid)
            {
                var PostCodes = GetPostCodes();

                foreach (PostCodeModel PCode in PostCodes)
                {
                    if (PCode.Suburb.ToLower() == PC.Suburb.ToLower())
                    {
                        throw new System.Web.Http.HttpResponseException(new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.InternalServerError));
                    }
                }

                var q = from pcode in PostCodes select pcode.id;
                var newId = q.Max() + 1;
                PC.id = newId;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                using (System.IO.StreamReader sr = new System.IO.StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/postcodes.csv")))
                {
                    sb.AppendLine(sr.ReadToEnd());
                }
                sb.Append(PC.id.ToString() + "," + PC.Suburb.ToString() + "," + PC.PostCode.ToString());
                using (System.IO.StreamWriter sw = new System.IO.StreamWriter(System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/postcodes.csv")))
                {
                    sw.Write(sb.ToString());
                }


            }
            else
            {
                throw new System.Web.Http.HttpResponseException(new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.InternalServerError));
            }
        }

    }
}

